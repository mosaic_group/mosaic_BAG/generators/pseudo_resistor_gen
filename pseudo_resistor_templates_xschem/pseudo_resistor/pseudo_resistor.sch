v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 220 -100 220 -80 {
lab=CNT}
N 120 -290 190 -290 {
lab=I}
N 120 -290 120 -140 {
lab=I}
N 120 -140 190 -140 {
lab=I}
N 250 -140 320 -140 {
lab=On}
N 250 -290 320 -290 {
lab=Op}
N 220 -360 220 -330 {
lab=CNT}
N 220 -80 220 -70 {
lab=CNT}
N 80 -210 120 -210 {
lab=I}
N 220 -180 220 -140 {
lab=VDD}
N 220 -290 220 -240 {
lab=VDD}
N 220 -190 220 -180 {
lab=VDD}
N 220 -200 220 -190 {
lab=VDD}
N 220 -240 220 -230 {
lab=VDD}
N 510 -180 510 -160 {
lab=VSS}
N 510 -130 560 -130 {
lab=VSS}
N 440 -130 470 -130 {
lab=VSS}
N 510 -100 510 -70 {
lab=VSS}
N 560 -130 570 -130 {
lab=VSS}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 220 -310 3 1 {name=XP0
w=4
l=18n
nf=2
model=pmos4_lvt
spiceprefix=X
}
C {devices/iopin.sym} 120 -370 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 120 -350 2 0 {name=p6 lab=VSS}
C {devices/lab_pin.sym} 220 -200 2 0 {name=l1 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 220 -230 2 0 {name=l2 sig_type=std_logic lab=VDD}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 490 -130 0 0 {name=XDUM
w=4
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 510 -180 2 0 {name=l3 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 440 -130 0 0 {name=l4 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 510 -70 0 0 {name=l5 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 570 -130 2 0 {name=l6 sig_type=std_logic lab=VSS}
C {devices/iopin.sym} 120 -330 2 0 {name=p7 lab=On}
C {devices/iopin.sym} 120 -310 2 0 {name=p8 lab=Op}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 220 -120 3 0 {name=XP1
w=4
l=90n
nf=2
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 320 -290 2 0 {name=l7 sig_type=std_logic lab=Op}
C {devices/lab_pin.sym} 320 -140 2 0 {name=l8 sig_type=std_logic lab=On}
C {devices/iopin.sym} 60 -390 0 0 {name=p4 lab=CNT}
C {devices/lab_pin.sym} 220 -360 2 0 {name=l9 sig_type=std_logic lab=CNT}
C {devices/lab_pin.sym} 220 -70 2 0 {name=l10 sig_type=std_logic lab=CNT}
C {devices/iopin.sym} 80 -410 2 0 {name=p1 lab=I}
C {devices/lab_pin.sym} 80 -210 0 0 {name=l11 sig_type=std_logic lab=I}
