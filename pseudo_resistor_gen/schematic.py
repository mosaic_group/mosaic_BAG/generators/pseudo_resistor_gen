import os
from typing import *
from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/pseudo_resistor_templates', 'netlist_info', 'pseudo_resistor.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library pseudo_resistor_templates cell pseudo_resistor.

    Fill in high level description here.
    """
    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            num_res_series='number of resistors in series',
            lch='inverter/switch channel length, in meters.',
            tx_info='List of dictionaries of transistor information',
            dum_info='Dummy information data structure.', 
            )

    def design(self,
               num_res_series: int,
               lch: float,
               tx_info: Dict[str, Dict[str, Any]],
               dum_info: List[Tuple[Any]]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        name_list_bot = ['XPB_' + str(x) for x in range(0, num_res_series)]
        name_list_top = ['XPT_' + str(x) for x in range(0, num_res_series)]
        term_list_top = []
        term_list_bot = []
        for idx in range(num_res_series):
            term_dict_top = {}
            term_dict_bot = {}
            term_dict_top['G'] = 'CNT'
            term_dict_bot['G'] = 'CNT'
            if idx == 0:
                term_dict_top['S'] = 'I'
                term_dict_bot['S'] = 'I'
            else:
                term_dict_top['S'] = 'mid_top%d' % (idx - 1)
                term_dict_bot['S'] = 'mid_bot%d' % (idx - 1)
            if idx == num_res_series - 1:
                term_dict_top['D'] = 'Op'
                term_dict_bot['D'] = 'On'
            else:
                term_dict_top['D'] = 'mid_top%d' % idx
                term_dict_bot['D'] = 'mid_bot%d' % idx
            term_list_top.append(term_dict_top)
            term_list_bot.append(term_dict_bot)

        w = tx_info['p']['w']
        th = tx_info['p']['th']
        nf = tx_info['p']['fg']

        self.instances['XP0'].design(w=w, l=lch, nf=nf, intent=th, multi=1)
        self.instances['XP1'].design(w=w, l=lch, nf=nf, intent=th, multi=1)

        self.array_instance('XP0', name_list_bot, term_list_bot)
        self.array_instance('XP1', name_list_top, term_list_top)

        # design dummies
        self.design_dummy_transistors(dum_info, 'XDUM', 'VDD', 'VSS')
