from typing import *
from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID

from sal.row import *
from sal.transistor import *

from .params import pseudo_resistor_layout_params


class layout(AnalogBase):
    """A Pseudo Resistor (PMOS).

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='pseudo_resistor_layout_params parameter object',
        )

    def draw_layout(self):
        params: pseudo_resistor_layout_params = self.params['params']

        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information
        row_Dummy_PB = Row(name='Dummy_PB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PB'],
                           threshold=params.th_dict['Dummy_PB'],
                           wire_names_g=['CNT'],
                           wire_names_ds=['IN', 'OUT']
                           )

        row_pmos1 = Row(name='p',
                        orientation=RowOrientation.R0,
                        channel_type=ChannelType.P,
                        width=params.w_dict['p'],
                        threshold=params.th_dict['p'],
                        wire_names_g=['CNT'],
                        wire_names_ds=['IN', 'OUT', 'I']
                        )

        row_pmos2 = Row(name='p',
                        orientation=RowOrientation.R0,
                        channel_type=ChannelType.P,
                        width=params.w_dict['p'],
                        threshold=params.th_dict['p'],
                        wire_names_g=['sig1', 'CNT'],
                        wire_names_ds=['IN', 'OUT']
                        )

        row_Dummy_PT = Row(name='Dummy_PT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PT'],
                           threshold=params.th_dict['Dummy_PT'],
                           wire_names_g=['CNT'],
                           wire_names_ds=['IN', 'OUT']
                           )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_Dummy_PB, row_pmos1, row_pmos2, row_Dummy_PT])

        # Initialize the transistors in the design
        # Compose a list of all the transistors, so it can be iterated over later

        divide = 1
        fg_p = params.seg_dict['p'] // divide

        TRANSISTORS_PER_ROW_COUNT = 6
        transistors_row1 = []
        transistors_row2 = []
        for idx in range(TRANSISTORS_PER_ROW_COUNT):
            pmos1 = Transistor(name='p', row=row_pmos1, fg=fg_p, seff_net='I', deff_net='Op')
            transistors_row1.append(pmos1)
            pmos2 = Transistor(name='p', row=row_pmos2, fg=fg_p, seff_net='I', deff_net='On')
            transistors_row2.append(pmos2)
        transistors = transistors_row1 + transistors_row2

        # 3:   Calculate transistor locations
        fg_single = transistors[0].fg
        fg_total = 6 * fg_single + 2 * params.ndum
        fg_dum = params.ndum

        for idx in range(TRANSISTORS_PER_ROW_COUNT):
            offset = fg_dum + idx * fg_single

            # Calculate positions of transistors
            transistors_row1[idx].assign_column(offset=offset, fg_col=fg_single, align=TransistorAlignment.CENTER)
            transistors_row2[idx].assign_column(offset=offset, fg_col=fg_single, align=TransistorAlignment.CENTER)

            # 4:  Assign the transistor directions (s/d up vs down)
            transistors_row1[idx].set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.MIDDLE)
            transistors_row2[idx].set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.MIDDLE)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        # 6:  Define horizontal tracks on which connections will be made
        row_pmos1_idx = rows.index_of_same_channel_type(row_pmos1)
        row_pmos2_idx = rows.index_of_same_channel_type(row_pmos2)
        tid_pmos1_CNT = self.get_wire_id('pch', row_pmos1_idx, 'g', wire_name='CNT')
        tid_pmos2_CNT = self.get_wire_id('pch', row_pmos2_idx, 'g', wire_name='CNT')
        tid_pmos1_I = self.get_wire_id('pch', row_pmos1_idx, 'ds', wire_name='I')
        tid_pmos1_IN = self.get_wire_id('pch', row_pmos1_idx, 'ds', wire_name='IN')
        tid_pmos1_OUT = self.get_wire_id('pch', row_pmos1_idx, 'ds', wire_name='OUT')
        tid_pmos2_IN = self.get_wire_id('pch', row_pmos2_idx, 'ds', wire_name='IN')
        tid_pmos2_OUT = self.get_wire_id('pch', row_pmos2_idx, 'ds', wire_name='OUT')

        # 7:  Perform wiring

        warr_p1_I = self.connect_to_tracks(
            [
                transistors_row1[0].s,
                transistors_row2[0].s
            ],
            tid_pmos1_I
        )
        warr_p1_G: Union[Optional[WireArray], Tuple[Optional[WireArray], List[WireArray]]] = None
        warr_p2_G: Union[Optional[WireArray], Tuple[Optional[WireArray], List[WireArray]]] = None
        warr_p1_d: Union[Optional[WireArray], Tuple[Optional[WireArray], List[WireArray]]] = None
        warr_p2_d: Union[Optional[WireArray], Tuple[Optional[WireArray], List[WireArray]]] = None

        for idx in range(TRANSISTORS_PER_ROW_COUNT):
            if idx > 0:
                warr_p1_G = self.connect_to_tracks(
                    [
                        transistors_row1[idx].g,
                        transistors_row1[idx - 1].g
                    ],
                    tid_pmos1_CNT
                )
                warr_p2_G = self.connect_to_tracks(
                    [
                        transistors_row2[idx].g,
                        transistors_row2[idx - 1].g
                    ],
                    tid_pmos2_CNT
                )
                tid_G_vert = TrackID(
                    layer_id=vert_conn_layer,
                    track_idx=self.grid.coord_to_nearest_track(
                        layer_id=vert_conn_layer,
                        coord=self.layout_info.col_to_coord(
                            col_idx=fg_dum + idx * fg_single,
                            unit_mode=True
                        ),
                        half_track=True,
                        mode=1,
                        unit_mode=True
                    ),
                    width=tr_manager.get_width(vert_conn_layer, 'sig1')
                )

                # Connection of all transistors gate
                self.connect_to_tracks(
                    [warr_p1_G, warr_p2_G],
                    tid_G_vert,
                    min_len_mode=0)

            if idx % 2 == 0:
                warr_p1_d = self.connect_to_tracks([transistors_row1[idx].d], tid_pmos1_IN)
                warr_p1_s = self.connect_to_tracks([transistors_row1[idx].s], tid_pmos1_OUT)
                warr_p2_d = self.connect_to_tracks([transistors_row2[idx].d], tid_pmos2_IN)
                warr_p2_s = self.connect_to_tracks([transistors_row2[idx].s], tid_pmos2_OUT)
            else:
                warr_p1_s = self.connect_to_tracks([transistors_row1[idx].s], tid_pmos1_IN)
                warr_p1_d = self.connect_to_tracks([transistors_row1[idx].d], tid_pmos1_OUT)
                warr_p2_s = self.connect_to_tracks([transistors_row2[idx].s], tid_pmos2_IN)
                warr_p2_d = self.connect_to_tracks([transistors_row2[idx].d], tid_pmos2_OUT)

        # Add Pin
        self.add_pin('I', [warr_p1_I], show=params.show_pins)
        self.add_pin('CNT', [warr_p1_G, warr_p2_G], show=params.show_pins)
        self.add_pin('Op', warr_p1_d, show=params.show_pins)
        self.add_pin('On', warr_p2_d, show=params.show_pins)

        # draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()
        # export supplies
        self.add_pin('VSS', ptap_wire_arrs)
        self.add_pin('VDD', ntap_wire_arrs)

        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            num_res_series=TRANSISTORS_PER_ROW_COUNT,
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class pseudo_resistor(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
